- [Building the Lighthouse Lamp](#building-the-lighthouse-lamp)
  - [Ingredients](#ingredients)
  - [Idea](#idea)
  - [LED Library](#led-library)
  - [Progress](#progress)
    - [Create the PlatformIO (PIO) project](#create-the-platformio-pio-project)

# Building the Lighthouse Lamp

The main functions of a lighthouse, to be an absolutely impressive structure and to guide ships safely through the dangerous sea, are guaranteed, among other things, by its lamp.\
And since this project is not building just any lighthouse, but the Arngaster, the lighting should be as true to the original as possible.



## Ingredients

To start, I built a little wood cylinder, got a APA102 LED Stripe and a NodeMCU to control the thing. I don‘t know if this is going to work. We will see.

— Wood cylinder built with 3 stacked planks

![Wood cylinder](./assets/wood-block.png "Wood cylinder")

- APA102 LED Stripe with 6 RGB LED/10cm → 60LED/100cm

![APA102 LED Stripe](assets/apa102ledstripe.png "APA102 LED Stripe")

- NodeMCUv3 from Berrybase

![Berrybase NodeMCUv3](assets/0b12461a8e5780aa044965dc1836b217.png "Berrybase NodeMCUv3")


## Idea

The current idea is the following..

I want to wrap the LED stripe around the wooden cylinder to cover almost 360°. Then I would like to use software to control the individual LEDs so that they imitate the lighting technology of the Arngast lighthouse.\


## LED Library


There are a few to select and for now I rely on the [NeoPixelBus](https://github.com/Makuna/NeoPixelBus/) Library.\
In its [wiki](https://github.com/Makuna/NeoPixelBus/wiki/Library-Comparisons) they compare the top 3 libraries:


There are multiple competing libraries, FastLED being the biggest and Adafruit NeoPixel being the most common for beginners.

On ESP8266, your primary choices are:

    NeoPixelBus
        Smaller than FastLED, more features and pixel support than esp8266_ws2812_i2s
        On Esp8266 you can choose i2s DMA or UART, both avoiding interrupts (NMIs). FastLED uses interrupts which can be problematic if you use other code that relies on interrupts, or wifi (although FastLED allows other fast interrupts to fire in between pixel updates)
        Supports RGBW pixels (not supported by the other 2 libraries) https://github.com/JoDaNl/esp8266_ws2812_i2s/
        Uses I2S interface to drive Neopixels via DMA providing an asynchronous update.
        Can use UART both in a synchronous and asynchronous model, but asynchronous limits the use of other UART libraries.
        Low level API with other features exposed by external classes.
        Pins available for use varies by platform due to hardware limitations.
        ESP32 support for using both RMT and i2s. RMT timing currently is sensitive to high interrupt frequency due to issues in the Core.
    FastLED
        Very rich API, although at a cost of large code and memory size
        Interrupt driven on ESP8266, so it's sensitive to timing.
        ESP32 support uses RMT which currently is sensitive to timing.
    Adafruit::NeoPixel
        Basic bit bang and interrupt driven library which does not support any other interrupt driven code to work. Not recommended.

On ESP32, both FastLED and NeoPixelBus can provide more than one channel/bus. FastLED primarily uses RMT to support 8 parallel channels. NeoPixelBus now supports the RMTs 8 channels and two more channels using i2s. Parallel channels provides for better refresh rate on longer strings (useful past 256 pixels). But do note that the latest cores have issues with high interrupt frequency causing timing issues.
Further, if you have 4096 pixels or more, you can use the 16 way output driver from Yves as described on this page



## Progress

1. Doing only one Row of LEDs for start to create the light pattern. Later decision if I duplicate the LED with wiring or with software. Currently about 10 LEDs but maybe less required.
2. I am going to use the PlatformIO Addon for VSCode to code and flash the NodeMCU/ESP8266 with Arduino code.
3. Deciding which LED Library to choose or if this isn't needed. There are a few to select and for now I rely on the [NeoPixelBus](https://github.com/Makuna/NeoPixelBus/) Library. Further information under [LED Library](#led-library)
4. Connect the LED Stripe to the ESP


### Create the PlatformIO (PIO) project

1. Start the PlatformIO Addon and select under PIO Home --> Projects & Configuration
2. Create a new Project with a name, your Board and the location to store the project.

My parameters:

| Key      | Value           |
|----------|-----------------|
| Name     | lighthouse-lamp |
| Board    | NodeMCU 1.0     |
| Location | My git folder   |


3. PlatformIO creates a new Folder at the selected location with the selected "Name" and with a few subfolders and files:

```bash
── lighthouse-lamp
│   ├── include
│   │   └── README
│   ├── lib
│   │   └── README
│   ├── platformio.ini
│   ├── src
│   │   └── main.cpp
│   └── test
│       └── README
```

The `platform.ini` file is the config file for this project with the parameters selected in step 2:

```bash
❯ bat lighthouse-lamp/platformio.ini
───────┬────────────────────────────────────────────────────────────────
       │ File: lighthouse-lamp/platformio.ini
       │ Size: 444 B
───────┼────────────────────────────────────────────────────────────────
   1   │ ; PlatformIO Project Configuration File
   2   │ ;
   3   │ ;   Build options: build flags, source filter
   4   │ ;   Upload options: custom upload port, speed and extra flags
   5   │ ;   Library options: dependencies, extra library storages
   6   │ ;   Advanced options: extra scripting
   7   │ ;
   8   │ ; Please visit documentation for the other options and examples
   9   │ ; https://docs.platformio.org/page/projectconf.html
  10   │ 
  11   │ [env:nodemcuv2]
  12   │ platform = espressif8266
  13   │ board = nodemcuv2
  14   │ framework = arduino
───────┴────────────────────────────────────────────────────────────────
```
