# Using the WLED Project

https://github.com/Aircoookie/WLED

## Install the WLED Binary
https://kno.wled.ge/basics/install-binary/

1. Download the current binary:
https://github.com/Aircoookie/WLED/releases

2. Flashing the NodeMCU with `esptool.py`:

```bash
❯ esptool.py --port /dev/ttyUSB1 write_flash 0x0 ./WLED_0.13.1_ESP8266.bin
esptool.py v2.8
Serial port /dev/ttyUSB1
Connecting....
Detecting chip type... ESP8266
Chip is ESP8266EX
Features: WiFi
Crystal is 26MHz
MAC: 30:83:98:ab:25:7f
Uploading stub...
Running stub...
Stub running...
Configuring flash size...
Auto-detected Flash size: 4MB
Compressed 747008 bytes to 505270...
Wrote 747008 bytes (505270 compressed) at 0x00000000 in 44.5 seconds (effective 134.2 kbit/s)...
Hash of data verified.

Leaving...
Hard resetting via RTS pin...

```

3. Connecting to the WLAN „WLED-AP“ with password `wled1234`
4. Go to the IP `4.3.2.1`

## Connect the LED Stripe

Connect the wires of the APA102 with the NodeMCU:

| LED Stripe   | NodeMCU Port | GPIO Mapping |
|--------------|--------------|--------------|
| GND (Ground) | GND          |              |
| C (Clock)    | D5           | GPIO14       |
| D (Data)     | D7           | GPIO13       |
| 5V (Power)   | -            | -            |

