# Lighthouse Arngast Lamp

- [Lighthouse Arngast Lamp](#lighthouse-arngast-lamp)
  - [General Description](#general-description)
  - [Description of the light](#description-of-the-light)
  - [The building process](#the-building-process)
  - [Licensing](#licensing)

## General Description

I am currently building a wood model of the lighthouse Arngast.
This repo contains files to describe the building and coding of the lamp and if applicable other lighting stuff like illuminating the windows.

Sooo, how does this lighthouse look in real life? Here you go:

![Lighthouse Arngast](./assets/lighthouse-arngast.png "Lighthouse Arngast")

Its place is in the „Jadebusen“, German North Sea Coast:

- Coordinates: [53° 28′ 53″ N, 8° 10′ 54″ O](https://geohack.toolforge.org/geohack.php?pagename=Leuchtturm_Arngast&language=de&params=53.481388888889_N_8.1816666666667_E_dim:50_region:DE-NI_type:building)
- OpenStreetMap entry: https://www.openstreetmap.org/way/294104505
- Wikipedia: https://de.wikipedia.org/wiki/Leuchtturm_Arngast
- WSA Wilhelmshaven: https://www.wsa-weser-jade-nordsee.wsv.de/Webs/WSA/Weser-Jade-Nordsee/DE/Wasserstrassen/BauwerkeSeezeichen/Schifffahrtszeichen/Leuchttuerme/Arngast/arngast_text.html
- Great video about the lighthouse: https://tube.tchncs.de/w/65YPG8WQPymHMGfNRbZu3E

<iframe title="Leuchtturm Arngast - Sendung mit der Maus" src="https://tube.tchncs.de/videos/embed/292fafb4-c10d-4d66-a035-40d6085eba32" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>


And what is the current state of my selfbuild model?

![Lighthouse Arngast selfbuild](./assets/IMG_20211124_173958.jpg "Lighthouse Arngast selfbuild")

There are plans to write a documentation about the progress to get to this state with taken pics etc.. But since time sadly isn‘t limitless and the actual work on the project is more fun for me than writing things down, bear with me if this does not happen after all.

## Description of the light

The lighthouse Arngast has white, green and red light at 30 metres above NHN(standard elevation zero). The white light flashes with the identifier. Then there are sectors with both flashing and permanent green light and permanent red light.

According to [OpenSeaMap](https://map.openseamap.org/?zoom=18&lat=53.48186&lon=8.18127&mlat=53.48133&mlon=8.18162&mtext=Lighthouse%20Arngast&layers=BFTFFFFFFTF0FFFFFFFFFF) there are a few sectors and I still don't fully understand how the lighthouse lights the world.

![](./assets/2022-05-03-14-42-35.png)


So what means what? Good question.. I'll try to answer that now:

| Description                                   | Chart Abbreviation |
|-----------------------------------------------|--------------------|
| Fixed white light                             | F.W                |
| Fixed green light                             | F.G                |
| Fixed red light                               | F.R                |
| Flashing white green                          | Fl.WG              |
| Flashing white with 2 flashes every 9 seconds | Fl(2)W.9s          |
| Occulting white                               |                    |

[Wasserschifffahrtsamt Wilhelmshaven](
https://www.wsa-weser-jade-nordsee.wsv.de/Webs/WSA/Weser-Jade-Nordsee/DE/Wasserstrassen/BauwerkeSeezeichen/Schifffahrtszeichen/Leuchttuerme/Arngast/arngast_text.html):

|  Leuchtfeuertechnische Angaben:   |  Hauptfeuer   |   Nebenfeuer  |
| --- | --- | --- |
| Feuerhöhe: | NN +31,62 m | NN \+ 31,62 m |
| Lichtquelle: | 1.600 W-Lampe | 1.600 W-Lampe |
| Lichtstärke: | 151.500 cd | 151.500 cd |
| Tragweite: | 21,0/16,1/17,2 sm;<br><br>21,0 sm;  <br>20,2/16,4 sm;  <br>20,2 sm | 10,1/7.5 sm |
| Kennung: | Fest, weiß, rot, grün;  <br>Unterbrochen, weiß;  <br>Blitz, weiß, grün;  <br>Blitz (2), weiß | Fest, weiß, grün |

https://www.deutsche-leuchtfeuer.de/nordsee/arngast.html:

|          |                                                                                                                                                                                                                                                                                                |
|----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Kennung: | F WRG    <br>W 135°-142°, G -150°, W -152°,  <br>G -174,6°, R 180,5°-191°, W -213°,  <br>R -225°, W 286°-303°, G -314°<br><br>Fl WG 3 s   0,8+(2,2) s  <br>G 174,6°-175,5°, W -176,4°<br><br>Fl(2) W 9 s   0,8+(2,2)+0,8+(5,2) s   <br>177,4°-180,5°<br><br>Oc W 6 s   1+(5) s   176,5°-177,4° |

https://de.wikipedia.org/wiki/Leuchtturm_Arngast:


|     | Hauptfeuer | Nebenfeuer |
| --- | --- | --- |
| Feuerhöhe | NN + 31,62 m | NN + 31,62 m |
| Lichtquelle | 1.600 Watt-Lampe | 1.600 Watt-Lampe |
| Lichtstärke in [cd](https://de.wikipedia.org/wiki/Candela "Candela") | 151.500 | 151.500 |
| Nenntragweite in [sm](https://de.wikipedia.org/wiki/Seemeile "Seemeile") | weiß 21; rot 16; grün 17 | weiß 10,1; grün 7,5 |
| Einteilung | 12 Sektoren |     |
| Kennungen |     |     |
| Festfeuer | weiß, rot, grün | weiß, grün |
| Blitzfeuer | weiß und grün, Blitz alle 3 Sekunden | –   |
| Blitzfeuer | weiß, 2 Blitze alle 9 Sekunden | –   |
| Unterbrochenes Feuer | alle 6 Sekunden | –   |


https://www.bsh.de/DE/PUBLIKATIONEN/_Anlagen/Downloads/Nautik_und_Schifffahrt/Sonstige-nautische-Publikationen/Wichtige-Zeichen-Abkuerzungen-Auswahl-Karte_1.pdf;jsessionid=834EB5832AB0C816AF567A6C831ACAD5.live11291?__blob=publicationFile&v=14:


![BSH - light description](assets/54cbdc42913f98615a2f2029b94fd89b.png "BSH - light description")


Further links with information about the light characteristic:

https://en.wikipedia.org/wiki/Light_characteristic


On this picture, the different sectors are good to see:

![7cd35276e9529e235aaaff8870a5792f.png](assets/7cd35276e9529e235aaaff8870a5792f.png)


*work in progress..*

I need to fix the access to my image backups to get to the required files (:


## The building process

Watch this file for the building progress of the lamp: [lighthouseLamp.md](lighthouseLamp.md)


## Licensing

1. The pictures in this repo are published  under the following license:\
[Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)

1. The other stuff, like text, code etc. is licensed under GPL3.
